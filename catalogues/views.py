from django.shortcuts import render
from .models import *

# Create your views here.

def catalogues(request):
    item = Barang.objects.all()
    # print(item)
    db = {'kunci' : item}
    return render(request, 'catalogues.html', {'kunci' : item})

def details(request, pk):
    item = Barang.objects.get(pk=pk)
    db = {'kunci' : item,}
    return render(request, 'details.html', {'kunci' : item})

