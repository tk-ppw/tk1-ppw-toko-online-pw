from django.db import models
from django.utils.translation import ugettext_lazy as _
from datetime import datetime 
from datetime import datetime, timedelta
from django.utils.timezone import make_aware
import pytz

def code():
    from random import choices
    from string import ascii_letters, digits
    code_coupon = ascii_letters[-26:] + digits
    return ''.join(choices(code_coupon, k=15))

class coupon_data(models.Model):
    minimum_price_in_rupiah_multiplied_by_1000 = models.PositiveIntegerField(default=0) 
    coupon_code = models.CharField(max_length=15, default=code)
    discount_percentage = models.PositiveIntegerField(default=0)
    expired_date = models.DateTimeField(default=make_aware(datetime.now() + timedelta(days=1),\
         timezone=pytz.timezone("Asia/Jakarta")))
    def __str__(self): 
        return 'coupon discount ' + str(self.discount_percentage)
    class Meta:
        verbose_name = "coupon"



