# Generated by Django 3.0.2 on 2020-03-06 18:54

import coupon.models
import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='coupon_data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('minimum_price_in_rupiah_multiplied_by_1000', models.PositiveIntegerField(default=0)),
                ('coupon_code', models.CharField(default=coupon.models.code, max_length=15)),
                ('discount_percentage', models.PositiveIntegerField(default=0)),
                ('expired_date', models.DateTimeField(default=datetime.datetime(2020, 3, 7, 18, 54, 36, 241060, tzinfo=utc))),
            ],
            options={
                'verbose_name': 'coupon',
            },
        ),
    ]
