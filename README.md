# PW Online Shop (Toko Online PW)

Tugas Kelompok 1 PPW 2019/2020 Term 2

[![pipeline status](https://gitlab.com/tk-ppw/pw-online-shop/badges/master/pipeline.svg)](https://gitlab.com/tk-ppw/pw-online-shop/-/commits/master)
[![coverage report](https://gitlab.com/tk-ppw/pw-online-shop/badges/master/coverage.svg)](https://gitlab.com/tk-ppw/pw-online-shop/-/commits/master)


2019/2020 - Tugas Kelompok 1 PPW

## Introduction

-URL : [PW Online Shop](http://toko-online-pw.herokuapp.com)<br>

- Anggota Kelompok :
    - Calmeryan Jireh - 1906292976
    - Fahdii Ajmalal Fikrie - 1906398370
    - Muhammad Aulia Adil Murtito - 1906398591
    - Radhiansya Zain Antriksa Putra - 1906398300