from django.db import models
from catalogues.models import Barang
from coupon.models import coupon_data
# Create your models here.
# class Barang(models.Model):
#     nama = models.CharField('Nama Barang', max_length=20)
#     harga = models.PositiveIntegerField('Harga')
#     stok = models.PositiveIntegerField('Stok')
    #deskripsi = models.TextField(blank=True)

    #def __str__(self):
    #    return self.nama
    #    
#class Kupon(models.Model):
#    kode = models.CharField('Kode Kupon',max_length=10)
    
class Transaksi(models.Model):
    pembeli = models.CharField('Nama Pembeli :',max_length=40)
    barang = models.CharField('Barang', max_length=10, default = "barang")
    #barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    kupon = models.CharField('Kupon', max_length=10)
    #kupon = models.ForeignKey(coupon_data, on_delete=models.CASCADE)
    total_harga = models.PositiveIntegerField('Harga', default=1)
    tanggal_beli = models.DateField('Tanggal beli')

    class Meta():
        ordering = ['tanggal_beli']