from django.test import TestCase,Client,override_settings
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from catalogues.views import Barang
from .views import histori_transaksi,transaksi
from .models import Transaksi
from .forms import TransaksiForm

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class TestTrf(TestCase): 
    def test_transaction_page_using_view_func(self):
        found = resolve('/transaksi/histori/')
        self.assertEqual(found.func, histori_transaksi)

    def test_object_coupon_if_exist(self):
        duar = Transaksi.objects.all().count()
        self.assertEqual(0, duar)

    def test_template(self):
        response = self.client.get(reverse('recent-trans'))
        self.assertTemplateUsed(response, 'histori.html')

    def test_Aktivitas_forms(self):
        form = TransaksiForm()
        form_data = {'pembeli': 'Calme','quantity': 1, 'coupon':"XXXX"}
        form = TransaksiForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_alldb_histori(self):
        prev = Transaksi.objects.all().count()
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        Transaksi.objects.create(pembeli = "Apis", barang = barang1.nama, kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        current = Transaksi.objects.all().count()
        self.assertEqual(current, prev+1)

    def test_cek_db_context(self):
        barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        duar = Transaksi.objects.create(pembeli = "Apis", barang = barang1.nama, kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        print(duar.tanggal_beli)
        prev = Transaksi.objects.all()
        self.assertIsInstance(duar, Transaksi)

    def test_erro_random_and_render_the_result(self):
        response_post = self.client.post(
            '/transaksi/sda', {
                'guest': 'badingsyalala'
            }
        )
        self.assertEqual(response_post.status_code, 404)
    
    def test_error_and_render_the_result(self):
        response_post = self.client.post(
            '/transaksi/pokpokpo', {
                'guest': 'badingsyalala'
            }
        )
        self.assertEqual(response_post.status_code, 404)

#     def test_hitorijanai(self):
#         barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
#         a = Transaksi.objects.create(pembeli = "Apis", barang = barang1.nama, kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
#         response = Transaksi.objects.all()
#         print(response)
#         self.assertContains(response, "Apis")

# class TestDuar(TestCase):
#     def setUp(self):
#         barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
#         trans = TransaksiForm(pembeli = "Fairuza", quantity = 1, coupon = "XXX")
#         trans.save()

#     def test_detail_mobil_url_is_exist(self):
#         x = TransaksiForm.objects.get(pk=1)
#         response = Client().post('/transaksi/1/', data={'x':x})
#         self.assertEqual(response.status_code, 200)

    #VIEWS
    # def test_histori_db(self):
        # orang = Client()
        # response = orang.post('/transaksi/histori/')
        # self.assertContains(response, 'Recent Transaction', status_code = 200)
# 
# 


# class TestDuar(TestCase):
    # def test_str_model_Barang(self):
        # event = Barang.objects.create(nama='pepewe menyenangkan')
        # member = Barang.objects.create(name="memberTest", event=event)
        # self.assertEqual(Barang.__str__(), 'pepewe menyenangkan')
    # def test_testi_template(self):
        # response = Client().get('/transaksi/histori')
        # self.assertTemplateUsed(response, 'histori.html')

    # def test_histori_db(self):
        # orang = Client()
        # response = orang.post('/transaksi/histori/')
        # self.assertContains(response, 'Recent Transaction', status_code = 200)
    # def test_alldb_histori(self):
        # prev = Transaksi.objects.all().count()
        # barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        # Transaksi.objects.create(pembeli = "Apis", barang = barang1.nama, kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        # current = models.Testimoni.objects.all().count()
        # self.assertEqual(current, prev+1)
    # def test_hitorijanai(self):
        # barang1 = Barang.objects.create(nama="Build Burning Gundam", harga = 250000, stok = 100)
        # a = Transaksi.objects.create(pembeli = "Apis", barang = barang1.nama, kupon = "XXX", total_harga = 200000, tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d"))
        # response = Transaksi.objects.all()
        # self.assertContains(response, "Apis")