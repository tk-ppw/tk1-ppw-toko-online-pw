from django import forms
from django.forms import ModelForm
from .models import Transaksi,Barang
from django.core.validators import MinValueValidator

class TransaksiForm(forms.Form):
    pembeli = forms.CharField(
        label = 'Nama Pembeli',
        required=True,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm'})
    )
    quantity = forms.IntegerField(
        label = "Qty",
        required = True,
        widget = forms.TextInput(attrs = {'class' : 'col-auto form-control form-control-sm'}),
        validators = [MinValueValidator(1)]
    )
    coupon = forms.CharField(
        label = "Coupon",
        widget = forms.TextInput(attrs = {'class' : 'col-auto form-control form-control-sm', 
        'placeholder' : 'if not use, type "not use"'})
    )
    class Meta:
        fields = ('pembeli','quantity','coupon')