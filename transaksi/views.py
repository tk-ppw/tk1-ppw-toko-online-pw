from django.shortcuts import render, redirect
from .forms import TransaksiForm
from .models import Transaksi
from catalogues.models import Barang
from coupon.models import coupon_data
from django.utils import timezone
from django.utils.timezone import make_aware
from datetime import datetime 
# Create your views here.
def histori_transaksi(request):
    histori = Transaksi.objects.all().order_by('-id')
    #print(histori)
    db = {
        'trans':histori
    }
    return render(request,'histori.html',context=db)

def transaksi(request,id_barang):
    
    my_dict={}
    obj = TransaksiForm()
    duar = Barang.objects.get(pk=id_barang)
    disk = coupon_data.objects.all()
    kupon = 0
    cek = False
    if request.method == "POST":
        form = TransaksiForm(request.POST)
        # print(form.data['pembeli'])
        # print(form.data['quantity'])
        a = form.data['coupon']
        #print(a)
        #print(duar.harga)
        price = int(form.data['quantity'])
        kupon = form.data['coupon']
        #price = int(form.data['quantity'])*duar.harga
        for i in disk:
            if a == i.coupon_code:
                cek = True
                kupon_used = i
                #print(kupon_used)
        if cek:
            if duar.harga >= 1000*kupon_used.minimum_price_in_rupiah_multiplied_by_1000:
                price = (1 - kupon_used.discount_percentage/100)*int(form.data['quantity'])*duar.harga
                if kupon_used.expired_date > make_aware(datetime.now()):
                    price = (1 - kupon_used.discount_percentage/100)*int(form.data['quantity'])*duar.harga
                #print(price)
        else:
            price = int(form.data['quantity'])*duar.harga

        if form.is_valid():
            obj = Transaksi(
            pembeli = form.data['pembeli'],
            barang = duar.nama,
            kupon = form.data['coupon'],
            total_harga = price,
            tanggal_beli = timezone.localtime(timezone.now()).strftime("%Y-%m-%d")
            )
            obj.save()
        return redirect('/transaksi/histori/')
    my_dict = {
        'form':obj,
        'duar':duar
    }
    return render(request,'transaksi.html',context=my_dict)