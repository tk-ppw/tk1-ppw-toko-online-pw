from django.test import TestCase, override_settings, Client
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .apps import LandingConfig
from .views import landing


# Create your tests here.
class TestLanding(TestCase):
    def test_apps(self):
        self.assertEqual(LandingConfig.name, 'landing')

    def test_landing_page_using_view_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)

    # def test_landing_page_is_exist(self):
    #     response = self.client.get('/')
    #     self.assertEqual(response.status_code, 200)


