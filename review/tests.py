from django.test import TestCase, override_settings, Client
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .apps import ReviewConfig
from .views import reviewPerProduct
from .models import Review
from catalogues.models import Barang


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class EventTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.barang = Barang.objects.create(nama='mabumabu', harga=100000, stok=10, deskripsi='sjsjsjsjsss')
        self.review = Review.objects.create(
            review_product=self.barang,
            review_name='sarmiji',
            review_city='DEPOKCITTY',
            review_star=5,
            review_review='HUAHUAHUHAUHAUA',
        )

    def test_apps(self):
        self.assertEqual(ReviewConfig.name, 'review')
        # self.assertEqual(review.get_app_config('review').name, 'review')

    # def test_event_page_using_view_func(self):
    #     found = resolve('/event/')
    #     self.assertEqual(found.func, events)

    # def test_event_page_is_exist(self):
    #     response = self.client.get('/event/')
    #     self.assertEqual(response.status_code, 200)

    # def test_event_page_post_success_and_render_the_result(self):
    #     response_post = self.client.post(
    #         '/event/', {
    #             'nama_event': 'Mabok',
    #             'deskripsi': 'asikasikan'
    #         }
    #     )
    #     self.assertEqual(response_post.status_code, 200)

    # def test_event_add_guest_page_post_success_and_render_the_result(self):
    #     response_post = self.client.post(
    #         '/event/add/Narkoba', {
    #             'guest': 'badingsyalala'
    #         }
    #     )
    #     self.assertEqual(response_post.status_code, 404)




    # def test_event_page_add_event_guests(self):
    #     new_act = Review.objects.create(nama_event="mabumabu")
    #     new_act.save()
    #     response = self.client.post('/event/add/mabumabu',{'guest':"buset"})
    #     self.assertEqual(response.status_code, 302)
